<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LinkMaisonLivre extends Model
{
    use HasFactory;
    protected $fillable = [
        'livre_id',
        'maison_id',
    ];
}
