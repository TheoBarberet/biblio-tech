<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;
    public $fillable = [
        'uuid',
        'type',
        'sender_user_id',
        'email',
        'maison_id',
        'status',
    ];
}
