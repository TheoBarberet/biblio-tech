<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Livre;
use Illuminate\Support\Facades\Auth;

class LivewireController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('canAccess',['except' => ['create','index']]);
        $this->middleware('userRole');
    }

    public function create(){
        return view('livres.create');
    }

    public function edit($livewire){
        $livre = Livre::where('id',$livewire)->first();
        return view('livres.edit')->with('livre',$livre);
    }

    public function index(){
        $livres = Livre::where('user_id',Auth::user()->id)->get();
        return view('livres.index')->with('livres',$livres);
    }
}
