<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Livre;
use App\Models\BuyLivre;
use App\Models\User;
use App\Notifications\BuyLivreNotification;
use Illuminate\Support\Facades\Auth;

class LivresController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('canDelete',['except' => ['show','create','store','edit','buy','index','wishlist','sell','catalogue']]);
        $this->middleware('livreExist',['except' => ['create','store','buy','index','wishlist','sell','catalogue']]);
        $this->middleware('userRole',['except' => ['wishlist','catalogue','show','buy']]);
    }

    public function show($livre){
        $leLivre = Livre::where('id',$livre)->first();
        return view('view')->with('livre',$leLivre);
    }

    public function create(){
        return view('livre');
    }

    public function edit($livre){
        $leLivre = Livre::where('id',$livre)->first();
        return view('livre')->with('livre',$leLivre);
    }

    public function wishlist(){
        $livresUser = BuyLivre::where('user_id',Auth::user()->id)->get();
        return view('wishlist')->with('livresUser',$livresUser);
    }

    public function sell(){
        $livres = Livre::where('user_id',Auth::user()->id)->get();
        return view('sell')->with('livres',$livres);
    }

    public function catalogue(){
        return view('catalogue');
    }

    public function buy($livre){
        $leLivre = Livre::where('id',$livre)->first();
        $achat = BuyLivre::create([
            'user_id' => Auth::user()->id,
            'livre_id' => $livre,
        ]);
        User::where('id',$achat->user_id)->first()->notify(new BuyLivreNotification($leLivre));
        return redirect()->back();
    }

    public function index(){
        $livres = Livre::where('user_id',Auth::user()->id)->get();
        return view('index')->with('livres',$livres);
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required|string|min:6',
            'description' => 'required|string|min:10',
            'nb_page' => 'required|numeric|between:100,999',
        ]);
            $livre = Livre::create([
                'user_id' => Auth::user()->id,
                'title' => request()->title,
                'description' => request()->description,
                'nb_page' => request()->nb_page
            ]);
            return redirect()->back();
    }

    public function update(Request $request, $livre){
        $request->validate([
            'title' => 'required|string|min:6',
            'description' => 'required|string|min:10',
            'nb_page' => 'required|numeric|between:100,999',
        ]);
        $leLivre = Livre::where('id',$livre)->first();
        $leLivre->update([
            'title'=>$request['title'],
            'description'=>$request['description'],
            'nb_page' => $request['nb_page'],
        ]);
        return redirect()->back();
    }

    public function destroy($livre){
        $leLivre = Livre::where('id',$livre)->first();
        $useless = BuyLivre::where('livre_id',$livre)->get();
        foreach($useless as $achat){
            $achat->delete();
        }
        $leLivre->delete();
        return redirect('/livres');
    }
}
