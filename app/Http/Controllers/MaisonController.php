<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MaisonController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('userRole');
    }
    public function index(){
        return view('indexMaison');
    }

    public function show($maison){
        return view('showMaison')->with('maison',$maison);
    }
}
