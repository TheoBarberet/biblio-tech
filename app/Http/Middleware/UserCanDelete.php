<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Livre;

class UserCanDelete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $uri_path = $_SERVER['REQUEST_URI'];
        $uri_parts = explode('/', $uri_path);
        $request_url = end($uri_parts);
        $livre = Livre::where('id',$request_url)->first();
        if($livre->user_id != Auth::user()->id){
            return redirect('/');
        }
        return $next($request);
    }
}
