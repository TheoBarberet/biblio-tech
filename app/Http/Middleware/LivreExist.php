<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Livre;

class LivreExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $livreId = $request->route()->parameter('livre');
        $livre = Livre::where('id',$livreId)->first();
        if(isset($livre)){
            return $next($request);
        }
        return redirect('home');
    }
}
