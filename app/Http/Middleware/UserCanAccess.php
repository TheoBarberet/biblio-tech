<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Livre;
use Illuminate\Support\Facades\Auth;

class UserCanAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $livre_id = $request->route()->parameter('livewire');
        if(Livre::where('id',$livre_id)->first()->user_id != Auth::user()->id){
            return redirect('/');
        }
        return $next($request);
    }
}
