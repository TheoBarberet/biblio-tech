<?php

namespace App\Http\Livewire;
use Livewire\Component;
use App\Models\Team;

class Wizard extends Component
{
    public $currentStep = 1;
    public $firstname, $lastname, $phone, $email, $schoolname,$detail, $website, $campusname, $campusadress, $codePostal;
    public $successMsg = '';

    /**
     * Write code on Method
     */
    public function render()
    {
        return view('livewire.wizard');
    }

    /**
     * Write code on Method
     */
    public function firstStepSubmit()
    {
        $validatedData = $this->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'phone' => 'required|numeric|max:9999999999|min:0000000000',
            'email' => 'required|email',
        ]);

        $this->currentStep = 2;
    }

    /**
     * Write code on Method
     */
    public function secondStepSubmit()
    {
        $validatedData = $this->validate([
            'schoolname' => 'required',
            'detail' => 'required',
            'website' => 'required|url',
        ]);

        $this->currentStep = 3;
    }

    public function thirdStepSubmit()
    {
        $validatedData = $this->validate([
            'campusname' => 'required',
            'campusadress' => 'required',
            'codePostal' => 'required|numeric|max:99999|min:00000',
        ]);

        $this->currentStep = 4;
    }

    /**
     * Write code on Method
     */
    public function submitForm()
    {
        $validatedData = $this->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'phone' => 'required|numeric|max:9999999999|min:0000000000',
            'email' => 'required|email',
            'schoolname' => 'required',
            'detail' => 'required',
            'website' => 'required|url',
            'campusname' => 'required',
            'campusadress' => 'required',
            'codePostal' => 'required|numeric|max:99999|min:00000',
        ]);

        Team::create([
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'phone' => $this->phone,
            'email' => $this->email,
            'schoolname' => $this->schoolname,
            'detail' => $this->detail,
            'website' => $this->website,
            'campusname' => $this->campusname,
            'campusadress' => $this->campusadress,
            'codePostal' => $this->codePostal,
        ]);

        $this->successMsg = 'Nouvelle école créée.';

        $this->clearForm();

        $this->currentStep = 1;
    }

    /**
     * Write code on Method
     */
    public function back($step)
    {
        $this->currentStep = $step;
    }

    /**
     * Write code on Method
     */
    public function clearForm()
    {
        $this->firstname = '';
        $this->lastname = '';
        $this->phone = '';
        $this->email = '';
        $this->schoolname = '';
        $this->detail = '';
        $this->website = '';
        $this->campusname = '';
        $this->campusadress = '';
        $this->codePostal = '';
    }
}
