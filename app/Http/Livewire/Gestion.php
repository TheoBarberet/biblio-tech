<?php

namespace App\Http\Livewire;

use App\Models\LinkMaisonUser;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Livre;
use App\Models\Type;
use Carbon\Carbon;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;

class Gestion extends Component
{
    public $type; // 1 : Create | 2 : Update | 3 : Modal
    public $livre;
    public $data;
    public $livreModalOpen=false;
    public $editModal=false;
    public $links;
    public $maison;
    public $categories;
    protected $listeners = ['callModal' => 'openModal', 'callCreateLivreModal' => 'openCreateLivreModal'];

    protected $rules = [
        'data.title' => 'required|string|min:6',
        'data.description' => 'required|string|min:10',
        'data.nb_page' => 'required|numeric|between:100,999',
        'data.maison_id' => 'required',
    ];

    public function mount(){
        if($this->livre!=null) {
            $leLivre = Livre::where('id',$this->livre)->first();
            $this->data = $leLivre->attributesToArray();
        }else {
            $this->data = [
                'title' => null,
                'description' => null,
                'nb_page' => null,
                'maison_id' => null
            ];
        }
    }


    public function createLivre(){
        $this->validate();
        $all = new MessageBag();
        $validatedData = Validator::make($this->data,[
            'title' => 'required|string|min:6',
            'description' => 'required|string|min:10',
            'nb_page' => 'required|numeric|between:100,999',
        ]);
        $all->merge($validatedData);
        if ($all->isNotEmpty()) {
            foreach ($all->getMessages() as $message => $error) {
                $this->addError($message, $error[0]);
            }
            return $this->getErrorBag();
        }
        $this->data['user_id'] = Auth::user()->id;
        $livre = Livre::create($this->data);
        $this->successMsg = 'Livre créé';
        return redirect('/livres');
    }

    public function openLivreModal(){
        $this->links = LinkMaisonUser::where('user_id',Auth::user()->id)->get();
        $this->livreModalOpen = true;
    }

    public function closeLivreModal(){
        $this->livreModalOpen = false;
    }

    public function openCreateLivreModal($maison){
        $this->maison = $maison;
        $this->categories = Type::all();
        $this->livreModalOpen = true;
    }

    public function openModal($id){
        $this->livre=Livre::where('id',$id)->first();
        $this->data = $this->livre->attributesToArray();
        $this->categories = Type::all();
        $this->editModal=true;
    }

    public function closeModal(){
        $this->editModal = false;
    }

    public function updateModal(){
        $this->validate();
        $this->data['created_at']=Livre::where('id',$this->data['id'])->first()->created_at;
        $this->data['updated_at']=Carbon::now();
        Livre::where('id',$this->data['id'])->update($this->data);
        $this->successMsg = 'Mis à jour';
        $this->emit('refreshListeLivres');
        $this->emit('refreshListeLivresMaison');
        $this->editModal = false;
    }

    public function closeAndClearModal(){
        $this->data = [
            'title' => null,
            'description' => null,
            'nb_page' => null
        ];
        $this->livreModalOpen = false;
    }

    public function createLivreModal(){
        $this->validate();
        $all = new MessageBag();
        $validatedData = Validator::make($this->data,[
            'title' => 'required|string|min:6',
            'description' => 'required|string|min:10',
            'nb_page' => 'required|numeric|between:100,999',
        ]);
        $all->merge($validatedData);
        if ($all->isNotEmpty()) {
            foreach ($all->getMessages() as $message => $error) {
                $this->addError($message, $error[0]);
            }
            return $this->getErrorBag();
        }
        $this->data['user_id'] = Auth::user()->id;
        $livre = Livre::create($this->data);
        if(isset($livre)){
            $this->livreModalOpen = false;
            $this->successMsg = 'Livre créé';
        $this->emit('refreshListeLivres');
        $this->data = [];
        }
    }

    public function createLivreMaisonModal(){
        $validatedData = Validator::make($this->data,[
            'title' => 'required|string|min:6',
            'description' => 'required|string|min:10',
            'nb_page' => 'required|numeric|between:100,999',
            'price' => 'required|numeric|between:5,50',
            'type' => 'required',
        ]);
        $this->data['user_id'] = Auth::user()->id;
        $this->data['maison_id'] = $this->maison;
        $livre = Livre::create($this->data);
        if(isset($livre)){
            $this->livreModalOpen = false;
            $this->successMsg = 'Livre créé';
            $this->emit('refreshListeLivresMaison');
        $this->data = [];
        }
    }

    public function update(){
       $this->validate();
        Livre::find($this->livre)->update($this->data);
        $this->successMsg = 'Mis à jour';
    }

    public function test(){
        return;
    }

    public function render()
    {
        return view('livewire.gestion');
    }
}
