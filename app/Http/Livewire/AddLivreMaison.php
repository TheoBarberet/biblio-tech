<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Livre;

class AddLivreMaison extends Component
{
    public $livreModalOpen=false;
    public $edition;
    public $type;
    public $data;
    public $livre;
    protected $listeners = ['callModalMaison' => 'openTheModal'];


    protected $rules = [
        'data.title' => 'required|string|min:6',
        'data.description' => 'required|string|min:10',
        'data.nb_page' => 'required|numeric|between:100,999',
        'data.maison_id' => 'required',
    ];

    public function mount(){
        if($this->livre!=null) {
            $leLivre = Livre::where('id',$this->livre)->first();
            $this->data = $leLivre->attributesToArray();
        }else {
            $this->data = [
                'title' => null,
                'description' => null,
                'nb_page' => null,
                'maison_id' => null
            ];
        }
    }

    public function openTheModal($id){
        $this->edition = $id;
        $this->livreModalOpen = true;
    }

    public function closeLivreModal(){
        $this->livreModalOpen = false;
    }

    public function closeAndClearModal(){
        $this->data = [
            'title' => null,
            'description' => null,
            'nb_page' => null
        ];
        $this->livreModalOpen = false;
    }

    public function createLivreModal($id){
        $this->validate();
        $all = new MessageBag();
        $validatedData = Validator::make($this->data,[
            'title' => 'required|string|min:6',
            'description' => 'required|string|min:10',
            'nb_page' => 'required|numeric|between:100,999',
        ]);
        $all->merge($validatedData);
        if ($all->isNotEmpty()) {
            foreach ($all->getMessages() as $message => $error) {
                $this->addError($message, $error[0]);
            }
            return $this->getErrorBag();
        }
        $this->data['user_id'] = Auth::user()->id;
        $this->data['maison_id'] = $this->edition;
        $livre = Livre::create($this->data);
        if(isset($livre)){
            $this->livreModalOpen = false;
            $this->successMsg = 'Livre créé';
        $this->emit('refreshListeLivres');
        $this->data = [];
        }
    }

    public function render()
    {
        return view('livewire.add-livre-maison');
    }
}
