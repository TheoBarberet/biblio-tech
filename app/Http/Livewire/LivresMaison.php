<?php

namespace App\Http\Livewire;

use App\Models\Livre;
use Livewire\Component;
use App\Models\BuyLivre;
use App\Models\User;
use App\Notifications\BuyLivreNotification;
use Illuminate\Support\Facades\Auth;

class LivresMaison extends Component
{
    public $livres;
    public $livrescatalogue;
    public $listeachats;
    public $livresvendu;
    public $maison;
    public $type;

    protected $listeners = ['refreshListeLivresMaison' => 'render'];

    public function openLivreModal($maison){
        $this->emit('callCreateLivreModal',$maison);
    }

    public function delete($id){
        $leLivre = Livre::where('id',$id)->first();
        $useless = BuyLivre::where('livre_id',$id)->get();
        foreach($useless as $achat){
            $achat->delete();
        }
        $leLivre->delete();
        $this->render();
    }

    public function buy($id){
        $leLivre = Livre::where('id',$id)->first();
        $achat = BuyLivre::create([
            'user_id' => Auth::user()->id,
            'livre_id' => $id,
        ]);
        User::where('id',$achat->user_id)->first()->notify(new BuyLivreNotification($leLivre));
    }

    public function unbuy($id){
        BuyLivre::where('id',$id)->delete();
        $this->render();
    }

    public function callModal($id){
        $this->emit('callModal',$id);
    }

    public function render()
    {
        $this->livres = Livre::where('maison_id',$this->maison)->get();
        $this->livrescatalogue = Livre::all();
        $this->listeachats = BuyLivre::where('user_id',Auth::user()->id)->get();
        $this->livresvendu = Livre::where('user_id',Auth::user()->id)->get();
        return view('livewire.livres-maison')->with('livres', $this->livres)->with('livrescatalogue', $this->livrescatalogue)->with('listeachats',$this->listeachats)->with('livresvendu',$this->livresvendu);
    }
}
