<?php

namespace App\Http\Livewire;

use App\Models\LinkMaisonUser;
use App\Models\MaisonEdition;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class CreateMaison extends Component
{
    protected $listeners = ['createNewMaison' => 'openMaisonModal'];
    public $openModalMaison=false;
    public $data;
    public $maison;

    protected $rules = [
        'data.name' => 'required|string|min:6',
        'data.slogan' => 'required|string|min:10',
    ];

    public function mount(){
        if($this->maison!=null) {
            $laMaison = MaisonEdition::where('id',$this->maison)->first();
            $this->data = $laMaison->attributesToArray();
        }else {
            $this->data = [
                'name' => null,
                'slogan' => null
            ];
        }
    }

    public function openMaisonModal(){
        $this->openModalMaison=true;
    }

    public function closeMaisonModal(){
        $this->openModalMaison=false;
    }

    public function closeAndClearModal(){
        $this->data = [
            'name' => null,
            'slogan' => null
        ];
        $this->openModalMaison=false;
    }

    public function createMaison(){
        $this->validate();
        $this->data['creator_id'] = Auth::user()->id;
        $maison = MaisonEdition::create($this->data);
        if(isset($maison)){
            LinkMaisonUser::create([
                'maison_id' => $maison->id,
                'user_id' => Auth::user()->id
            ]);
            $this->openModalMaison = false;
            $this->successMsg = 'Maison créée';
        $this->emit('refreshListMaison');
        $this->data = [];
        }
    }

    public function render()
    {
        return view('livewire.create-maison');
    }
}
