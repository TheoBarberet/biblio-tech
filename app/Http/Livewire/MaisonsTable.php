<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\MaisonEdition;
use App\Models\BuyLivre;
use App\Models\LinkMaisonUser;
use App\Models\Livre;
use Illuminate\Support\Facades\Auth;

class MaisonsTable extends Component
{
    public $maisons;
    protected $listeners = ['refreshListMaison' => 'render'];

    public function delete($id){
        $maison = MaisonEdition::where('id',$id)->first();
        $uselessLivre = Livre::where('maison_id',$id)->get();
        foreach($uselessLivre as $leLivre){
        $uselessAchat = BuyLivre::where('livre_id',$id)->get();
        foreach($uselessAchat as $achat){
            $achat->delete();
        }
        $leLivre->delete();
        }
        $links = LinkMaisonUser::where('maison_id',$id)->get();
        foreach($links as $link){
            $link->delete();
        }
        $maison->delete();
        $this->render();
    }

    public function callCreateMaisonModal(){
        $this->emit('createNewMaison');
    }

    public function render()
    {
        $this->maisons = MaisonEdition::all();
        return view('livewire.maisons-table')->with('maisons',$this->maisons);
    }
}
