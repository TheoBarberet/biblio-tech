<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Livre;
use App\Models\BuyLivre;

class LivresTable extends Component
{
    protected $listeners = ['refreshListeLivres' => 'render'];
    public $livres;

    public function delete($id){
        $leLivre = Livre::where('id',$id)->first();
        $useless = BuyLivre::where('livre_id',$id)->get();
        foreach($useless as $achat){
            $achat->delete();
        }
        $leLivre->delete();
        $this->render();
    }

    public function callModal($id){
        $this->emit('callModal',$id);
    }

    public function render()
    {
        $this->livres = Livre::where('user_id',Auth::user()->id)->get();
        return view('livewire.livres-table')->with('livres',$this->livres);
    }
}
