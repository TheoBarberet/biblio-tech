<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Livre;
use App\Models\MaisonEdition;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class EditMaison extends Component
{
    public $livres;
    public $maison;
    public $data;

    protected $rules = [
        'data.name' => 'required|string|min:6',
        'data.slogan' => 'required|string|min:10',
    ];

    public function mount(){
        if($this->maison!=null) {
            $laMaison = MaisonEdition::where('id',$this->maison)->first();
            $this->data = $laMaison->attributesToArray();
        }else {
            $this->data = [
                'name' => null,
                'slogan' => null
            ];
        }
    }

    public function updateMaison(){
        $this->validate();
        $this->data['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
        MaisonEdition::where('id',$this->maison)->first()->update($this->data);
    }

    public function render()
    {
        $this->livres = Livre::where('maison_id',$this->maison)->get();
        return view('livewire.edit-maison')->with('livres',$this->livres);
    }
}
