<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\models\Livre;
use App\models\BuyLivre;
use Illuminate\Notifications\Messages\BroadcastMessage;

class BuyLivreNotification extends Notification
{
    use Queueable;

    public $livre;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Livre $livre)
    {
        $this->livre = $livre;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'livre_id' =>$this->livre->id,
            'livre_title' =>$this->livre->title
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(['livre_id' => $this->livre->id]);
    }
}
