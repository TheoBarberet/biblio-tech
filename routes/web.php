<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CropImageController;
use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/dashboard', 'PagesController@index');
Route::get('/', 'PagesController@index');
Route::get('/theo', 'PagesController@theo');
Route::get('/test', 'PagesController@test');
Route::get('/register/{role}', 'Auth\RegisterController@showRegister');

Route::resource('livres', 'LivresController');
Route::resource('livewire','LivewireController');
Route::post('/buy/{livre}', 'LivresController@buy');
Route::get('/wishlist', 'LivresController@wishlist');
Route::get('/sell', 'LivresController@sell');
Route::get('/table', 'TableController@table');
Route::get('/catalogue', 'LivresController@catalogue');
Route::resource('maison', 'MaisonController');




// Demo routes
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/jquerymask', 'PagesController@jQueryMask');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


