"use strict";
// Class Definition
var KTLogin = function() {
    var _login;
    var _handleSignUpForm = function(e) {
        var validation;
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
            KTUtil.getById('kt_login_signup_form'),
            {
                fields: {
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email address is required'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        );
        $('#kt_login_signup_submit').on('click', function (e) {
            e.preventDefault();
            validation.validate().then(function(status) {
                if (status == 'Valid') {
                var formsend = $('#kt_login_signup_form');
                    $.ajax({
                        type: formsend.attr('method'),
                        url: formsend.attr('action'),
                        data: formsend.serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response) {
                            window.location.href = "./login";
                        }
                    });
                }
            });
        });
        // Handle cancel button
        $('#kt_login_signup_cancel').on('click', function (e) {
            e.preventDefault();
            window.location.replace("./login");
        });
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            _login = $('#kt_login');
            _handleSignUpForm();
        }
    };
}();
// Class Initialization
jQuery(document).ready(function() {
    KTLogin.init();
});
