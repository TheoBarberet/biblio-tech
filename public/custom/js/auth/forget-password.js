"use strict";
// Class Definition
var KTLogin = function() {
    var _login;
    var _handleForgotForm = function(e) {
        var validation;
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
            KTUtil.getById('kt_login_forgot_form'),
            {
                fields: {
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email address is required'
                            },
                            emailAddress: {
                                message: 'The value is not a valid email address'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap()
                }
            }
        );
        // Handle submit button
        $('#kt_login_forgot_submit').on('click', function (e) {
            e.preventDefault();
            validation.validate().then(function(status) {
                if (status == 'Valid') {
                    var formsend = $('#kt_login_forgot_form');
                    $.ajax({
                        type: formsend.attr('method'),
                        url: formsend.attr('action'),
                        data: formsend.serialize(),
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response) {
                            window.location.href = "./";
                        }
                    });
                }
            });
        });
        // Handle cancel button
        $('#kt_login_forgot_cancel').on('click', function (e) {
            e.preventDefault();
            window.location.replace("./login");
        });
    }
    // Public Functions
    return {
        // public functions
        init: function() {
            _login = $('#kt_login');
            _handleForgotForm();
        }
    };
}();
// Class Initialization
jQuery(document).ready(function() {
    KTLogin.init();
});
