@extends('layout.default')
@section('styles')
@livewireStyles
<style>
    table
{
    border-collapse: collapse;
}
td, th /* Mettre une bordure sur les td ET les th */
{
    border: 1px solid black;
}
</style>
@endsection
@section('content')
<div class="card card-custom gutter-b">
        @livewire('gestion', ['type' => '3'])
    @livewire('livres-table')
</div>


@endsection
@section('scripts')
@livewireScripts
@endsection


