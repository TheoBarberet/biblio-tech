@extends('layout.default')
@section('styles')
@livewireStyles
@endsection
@section('content')
<div>
    <!--begin::Container-->
    <div class="container">
        <div class="card card-custom rounded-top-0">
            <div class="card-header border-0 pt-5">
                <h3 class="card-title font-weight-bolder">Modification du livre {{$livre->title}}</h3>
            </div>
            <div class="card-body p-0">
                <div class="row justify-content-center py-8 px-8 py-lg-15 px-lg-10">
                    @livewire('gestion', ['type' => '2','livre'=>$livre->id])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@livewireScripts
@endsection
