@extends('layout.default')
@section('styles')
@livewireStyles
@endsection
@section('content')
<div>
    <!--begin::Container-->
    <div class="container">
        <div class="card card-custom rounded-top-0">
            <div class="card-header border-0 pt-5">
                <h3 class="card-title font-weight-bolder">Création d'un livre par l'utilisateur {{Auth::user()->name}}</h3>
            </div>
            <div class="card-body p-0">
                <div class="row justify-content-center py-8 px-8 py-lg-15 px-lg-10">
                    @livewire('gestion', ['type' => '1'])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@livewireScripts
<script>
    function checkTitle()
{
    setTimeout(function(){
    const length = $("#titre_livre").val().length;
    if(length>=6){
        $("#titre_livre").addClass("is-valid");
    }
    else{
        $("#titre_livre").removeClass("is-valid");
    }
    }, 20);
}

    function checkDescription()
{
    setTimeout(function(){
    const length = $("#description_livre").val().length;
    if(length>=10){
        $("#description_livre").addClass("is-valid");
    }
    else{
        $("#description_livre").removeClass("is-valid");
    }
    }, 20);
}

    function checkPages()
{
    setTimeout(function(){
    const length = $("#nb_pages_livre").val().length;
    let val = document.getElementById("nb_pages_livre").value
    let isnum = /^\d+$/.test(val);
    if(length==3){
        if(isnum){
        $("#nb_pages_livre").addClass("is-valid");
        }
        else{
        $("#nb_pages_livre").removeClass("is-valid");
        }
    }
    else{
        $("#nb_pages_livre").removeClass("is-valid");
    }
    }, 20);
}

</script>
@endsection
