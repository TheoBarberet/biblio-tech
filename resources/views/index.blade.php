@extends('layout.default')
<style>
    table
{
    border-collapse: collapse;
}
td, th /* Mettre une bordure sur les td ET les th */
{
    border: 1px solid black;
}
</style>
@section('content')
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">
                    Liste de vos livres
            </h3>
        </div>
        <button class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" onclick="window.location='{{ route('livewire.create') }}'">Créer un livre</button>
    </div>
    <div class="card-body">
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-9 col-xl-8">
                    <div class="row align-items-center">
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Rechercher..."
                                    id="kt_datatable_search_query_campuse" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                            <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Ok</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable_campuse">
    <thead>
        <tr>
            <th title="Field #1">Titre</th>
            <th title="Field #2">Description</th>
            <th title="Field #3">Pages</th>
            <th title="Field #4">Suppression</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($livres as $livre)
        <tr>
            <td><a href="/livres/{{$livre->id}}">{{ $livre->title }}</a></td>
            <td>{{ $livre->description }}</td>
            <td>{{ $livre->nb_page }}</td>
            <td>
                <form method="POST" action="/livres/{{$livre->id}}">
                    @csrf
                    @method('DELETE')
                    <button onclick="return confirm('Confirmer la suppression de {{$livre->title}} de la liste de vos livres ?')" type="submit" class="btn btn-sm btn-clean btn-icon" title="Supprimer">
                        <i class="flaticon-delete text-danger"></i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="/custom/js/kt_datatable/html-table-campuse.js">
</script>
@endsection
