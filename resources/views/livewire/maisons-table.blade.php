<div>
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">Liste des maisons d'édition</span>
                <span class="text-muted mt-3 font-weight-bold font-size-sm">Trouvez l'ensemble des maisons du site</span>
            </h3>
            @role('superuser')
            <div class="card-toolbar">
                <div class="text-right mr-5">
                    <button type="button" class="btn btn-primary btn-lg pull-right" wire:click="$emit('createNewMaison')">Créer une maison d'édition</button>
                </div>
                @livewire('create-maison')
            </div>
            @endrole
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th style="min-width: 100px" class="pl-7">
                                    <span class="text-dark-75">Nom</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Slogan</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Nom du créateur</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Action</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($maisons as $maison)
                            <tr>
                                <td class="pl-0 py-8">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <a class="text-dark-75 font-weight d-block font-size-lg" href="/maison/{{$maison->id}}">{{ $maison->name }}</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="text-dark-75 font-weight d-block font-size-lg">{{ $maison->slogan }}</p>
                                </td>
                                <td>
                                    <p class="text-dark-75 font-weight d-block font-size-lg">{{ User::where('id', $maison->creator_id)->first()->name }}</p>
                                </td>
                                <td>
                                    @if ($maison->creator_id == Auth::user()->id)
                                        <button wire:click='delete({{$maison->id}})' class="btn btn-sm btn-clean btn-icon" title="Supprimer">
                                            <i class="flaticon-delete text-danger"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end::Table-->
            </div>
        </div>
        <!--end::Body-->
    </div>
</div>
