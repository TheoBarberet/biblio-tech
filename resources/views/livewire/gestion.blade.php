<div>
    @if($type==3)
    <div class="text-right mr-5 mt-5">
    <button type="button" class="btn btn-dark btn-lg pull-right" wire:click="openLivreModal">Créer
        le livre</button>
    </div>
        @if ($livreModalOpen)
        <div class="modal-backdrop show"></div>
        <div class="modal d-block" tabindex="-1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Création de livres
                        </h5>
                        <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeLivreModal">
                            <span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12">
                                <!--begin::Input-->
                                <div class="form-group">
                                    <label>Titre :</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg"
                                        wire:model="data.title" placeholder="Titre du livre"/>
                                    <span class="form-text text-muted"></span>
                                    @error('data.title') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                    @enderror
                                </div>
                                <!--end::Input-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <!--begin::Input-->
                                <div class="form-group">
                                    <label>Description :</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg"
                                        wire:model="data.description" placeholder="Description du livre"/>
                                    <span class="form-text text-muted"></span>
                                    @error('data.description') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                    @enderror
                                </div>
                                <!--end::Input-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <!--begin::Input-->
                                <div class="form-group">
                                    <label>Nombre de pages</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg"
                                        wire:model="data.nb_page" placeholder="Nombre de pages du livre"/>
                                    <span class="form-text text-muted"></span>
                                    @error('data.nb_page') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                    @enderror
                                </div>
                                <!--end::Input-->
                            </div>
                        </div>
                        <div class="row">
                            <label>Choisissez une maison d'édition</label>
                            <select name="selectMaison" wire:model="data.maison_id"
                                class="form-control form-control-solid form-control-lg">
                                <option value="">Select</option>
                                @foreach ($links as $link)
                                <option value="{{$link->maison_id}}">{{MaisonEdition::where('id',$link->maison_id)->first()->name}}</option>
                                @endforeach
                            </select>
                            @error('data.maison_id') <span class="error" style="color:#FF0000">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeAndClearModal">
                            Annuler</button>
                        <button type="button" class="btn btn-success font-weight-bolder" wire:click="createLivreModal">
                            <span aria-hidden="true">Validez</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @elseif ($editModal)
    <div class="modal-backdrop show"></div>
    <div class="modal d-block" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Modification du livre {{$livre->title}}
                    </h5>
                    <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeModal">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Titre :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.title" placeholder="Titre du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.title') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Description :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.description" placeholder="Description du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.description') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Nombre de pages</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.nb_page" placeholder="Nombre de pages du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.nb_page') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary font-weight-bolder" wire:click="closeModal">
                        Annuler</button>
                    <button type="button" class="btn btn-success font-weight-bolder" wire:click="updateModal">
                        <span aria-hidden="true">Validez</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif
    @elseif ($type==4)
    @if ($livreModalOpen)
    <div class="modal-backdrop show"></div>
    <div class="modal d-block" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Création de livres
                    </h5>
                    <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeLivreModal">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Titre :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.title" placeholder="Titre du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.title') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Description :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.description" placeholder="Description du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.description') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Nombre de pages</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.nb_page" placeholder="Nombre de pages du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.nb_page') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Prix</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.price" placeholder="Prix du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.price') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <label>Choisissez la catégorie de votre livre</label>
                        <select name="selectType" wire:model="data.type"
                            class="form-control form-control-solid form-control-lg">
                            <option value="">Select</option>
                            @foreach ($categories as $categorie)
                            <option value="{{$categorie->name}}">{{$categorie->name}}</option>
                            @endforeach
                        </select>
                        @error('data.type') <span class="error" style="color:#FF0000">{{ $message }}</span>
                        @enderror
                    </div>
                    <input type="hidden" wire:model="data.maison_id" value="{{$maison}}"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeAndClearModal">
                        Annuler</button>
                    <button type="button" class="btn btn-success font-weight-bolder" wire:click="createLivreMaisonModal">
                        <span aria-hidden="true">Validez</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @elseif ($editModal)
    <div class="modal-backdrop show"></div>
    <div class="modal d-block" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Modification du livre {{$livre->title}}
                    </h5>
                    <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeModal">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Titre :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.title" placeholder="Titre du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.title') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Description :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.description" placeholder="Description du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.description') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Nombre de pages</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.nb_page" placeholder="Nombre de pages du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.nb_page') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Prix</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.price" placeholder="Prix du livre"/>
                                <span class="form-text text-muted"></span>
                                @error('data.price') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <label>Choisissez la catégorie de votre livre</label>
                        <select name="selectType" wire:model="data.type"
                            class="form-control form-control-solid form-control-lg">
                            <option value="">Select</option>
                            @foreach ($categories as $categorie)
                            <option value="{{$categorie->name}}">{{$categorie->name}}</option>
                            @endforeach
                        </select>
                        @error('data.type') <span class="error" style="color:#FF0000">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary font-weight-bolder" wire:click="closeModal">
                        Annuler</button>
                    <button type="button" class="btn btn-success font-weight-bolder" wire:click="updateModal">
                        <span aria-hidden="true">Validez</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif

    @endif

</div>
