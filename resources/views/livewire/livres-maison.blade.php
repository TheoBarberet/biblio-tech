<div>
    @if(isset($maison))
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">Liste des livres de la maison</span>
                <span class="text-muted mt-3 font-weight-bold font-size-sm">Livres propriétés de
                    {{ MaisonEdition::where('id', $maison)->first()->name }}</span>
            </h3>
            @role('superuser')
            <div class="card-toolbar">
                <div class="text-right mr-5">
                    @if (LinkMaisonUser::where('user_id', Auth::user()->id)->where('maison_id', $maison)->first() != null)
                        <div class="text-right">
                            <button type="button" class="btn btn-dark btn-lg pull-right"
                                wire:click="openLivreModal({{ $maison }})">Créer
                                le livre</button>
                        </div>
                    @endif
                </div>
            </div>
            @endrole
        </div>
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th style="min-width: 100px" class="pl-7">
                                    <span class="text-dark-75">Titre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Description</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Pages</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($livres as $livre)
                                <tr>
                                    <td class="pl-0 py-8">
                                        <div class="d-flex align-items-center">
                                            <a class="text-dark-75 font-weight d-block font-size-lg"
                                                href="/livres/{{ $livre->id }}">{{ $livre->title }}</a>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">
                                            {{ $livre->description }}</p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->nb_page }}
                                        </p>
                                    </td>
                                    <td>
                                        <button wire:click='delete({{$livre->id}})' class="btn btn-sm btn-clean btn-icon" title="Supprimer">
                                            <i class="flaticon-delete text-danger"></i>
                                        </button>
                                        <button wire:click='callModal({{$livre->id}})' class="btn btn-sm btn-clean btn-icon" title="Editer">
                                            <i class="flaticon2-edit text-primary"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @livewire('gestion', ['type' => '4'])
    </div>
    @else
    @if ($type==1)

    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">Liste des livres achetables</span>
                <span class="text-muted mt-3 font-weight-bold font-size-sm">Tous les articles de notre boutique sont dans cette liste</span>
            </h3>
        </div>
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th style="min-width: 100px" class="pl-7">
                                    <span class="text-dark-75">Titre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Description</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Pages</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Prix</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Nom de l'auteur</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Nom de la maison d'édition</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Type de livre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($livrescatalogue as $livre)
                                <tr>
                                    <td class="pl-0 py-8">
                                        <div class="d-flex align-items-center">
                                            <a class="text-dark-75 font-weight d-block font-size-lg"
                                                href="/livres/{{ $livre->id }}">{{ $livre->title }}</a>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">
                                            {{ $livre->description }}</p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->nb_page }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->price }}€
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ User::where('id',$livre->user_id)->first()->name }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ MaisonEdition::where('id',$livre->maison_id)->first()->name }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->type }}
                                        </p>
                                    </td>
                                    <td>
                                        <button wire:click='buy({{$livre->id}})' class="btn btn-sm btn-clean btn-icon" title="Acheter">
                                            <i class="flaticon-coins text-success"> Acheter</i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @elseif ($type==2)
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">Liste de mes achats</span>
                <span class="text-muted mt-3 font-weight-bold font-size-sm">Tous les articles achetés</span>
            </h3>
        </div>
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th style="min-width: 100px" class="pl-7">
                                    <span class="text-dark-75">Titre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Description</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Pages</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Prix</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Nom de l'auteur</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Nom de la maison d'édition</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Type de livre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($listeachats as $achat)
                                <tr>
                                    <td class="pl-0 py-8">
                                        <div class="d-flex align-items-center">
                                            <a class="text-dark-75 font-weight d-block font-size-lg"
                                                href="/livres/{{ $achat->livre_id }}">{{ Livre::where('id',$achat->livre_id)->first()->title }}</a>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">
                                            {{ Livre::where('id',$achat->livre_id)->first()->description }}</p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ Livre::where('id',$achat->livre_id)->first()->nb_page }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ Livre::where('id',$achat->livre_id)->first()->price }}€
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ User::where('id',Livre::where('id',$achat->livre_id)->first()->user_id)->first()->name }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ MaisonEdition::where('id',Livre::where('id',$achat->livre_id)->first()->maison_id)->first()->name }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ Livre::where('id',$achat->livre_id)->first()->type }}
                                        </p>
                                    </td>
                                    <td>
                                        <button wire:click='unbuy({{$achat->id}})' class="btn btn-sm btn-clean btn-icon" title="Annuler l'achat">
                                            <i class="flaticon2-delete text-danger"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @elseif($type==3)
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">Liste des livres vendus</span>
                <span class="text-muted mt-3 font-weight-bold font-size-sm">Tous vos livres vendus & quantité vendue</span>
            </h3>
        </div>
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th style="min-width: 100px" class="pl-7">
                                    <span class="text-dark-75">Titre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Description</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Pages</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Prix</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Nom de la maison d'édition</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Type de livre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Quantité</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($livresvendu as $livre)
                            @if (BuyLivre::where('livre_id',$livre->id)->count()!=0)
                                <tr>
                                    <td class="pl-0 py-8">
                                        <div class="d-flex align-items-center">
                                            <a class="text-dark-75 font-weight d-block font-size-lg"
                                                href="/livres/{{ $livre->id }}">{{ $livre->title }}</a>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">
                                            {{ $livre->description }}</p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->nb_page }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->price }}€
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ MaisonEdition::where('id',$livre->maison_id)->first()->name }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->type }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ BuyLivre::where('livre_id',$livre->id)->count() }}
                                        </p>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endif
</div>

