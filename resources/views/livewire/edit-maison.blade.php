<div>
    <div class="form-group row">
        <label class="col-3 col-form-label">
            <b>Nom :</b>
        </label>
        <div class="col-xs-3">
            <input class="form-control form-control-solid" type="text" @if(MaisonEdition::where('id',$maison)->first()->creator_id!=Auth::user()->id)readonly="readonly"@endif wire:model="data.name"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-3 col-form-label">
            <b>Slogan :</b>
        </label>
        <div class="col-xs-3">
            <input class="form-control form-control-solid" type="text" @if(MaisonEdition::where('id',$maison)->first()->creator_id!=Auth::user()->id)readonly="readonly"@endif wire:model="data.slogan"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-3 col-form-label">
            <b>Nom du créateur :</b>
        </label>
        <div class="col-xs-3">
            <input class="form-control form-control-solid" type="text" readonly="readonly" value="{{User::where('id',MaisonEdition::where('id',$maison)->first()->creator_id)->first()->name}}"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-3 col-form-label">
            <b>Créée le :</b>
        </label>
        <div class="col-xs-3">
            <input class="form-control form-control-solid" type="text" readonly="readonly" value="{{MaisonEdition::where('id',$maison)->first()->created_at}}"/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-3 col-form-label">
            <b>Modifié le :</b>
        </label>
        <div class="col-xs-3">
            <input class="form-control form-control-solid" type="text" readonly="readonly" value="{{MaisonEdition::where('id',$maison)->first()->updated_at}}"/>
        </div>
    </div>
    @if(MaisonEdition::where('id',$maison)->first()->creator_id==Auth::user()->id)
    <button type="button" class="btn btn-primary btn-lg pull-right" wire:click="updateMaison">Modifier</button>
    @endif
</div>
