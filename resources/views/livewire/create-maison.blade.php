<div>
    @if ($openModalMaison)
    <div class="modal-backdrop show"></div>
    <div class="modal d-block" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Création de Maison d'édition
                    </h5>
                    <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeMaisonModal">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Nom :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.name" placeholder="Nom de la maison"/>
                                <span class="form-text text-muted"></span>
                                @error('data.name') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <!--begin::Input-->
                            <div class="form-group">
                                <label>Slogan :</label>
                                <input type="text" class="form-control form-control-solid form-control-lg"
                                    wire:model="data.slogan" placeholder="Slogan de la maison"/>
                                <span class="form-text text-muted"></span>
                                @error('data.slogan') <span class="error" style="color:#FF0000">{{ $message }}</span>
                                @enderror
                            </div>
                            <!--end::Input-->
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger font-weight-bolder" wire:click="closeAndClearModal">
                        Annuler</button>
                    <button type="button" class="btn btn-success font-weight-bolder" wire:click="createMaison">
                        <span aria-hidden="true">Validez</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
