<div>
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">Liste des livres de la maison</span>
                <span class="text-muted mt-3 font-weight-bold font-size-sm">Livres propriétés de
                    {{ Auth::user()->first()->name }}</span>
            </h3>
        </div>
        <div class="card-body pt-0 pb-3">
            <div class="tab-content">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th style="min-width: 100px" class="pl-7">
                                    <span class="text-dark-75">Titre</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Description</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Pages</span>
                                </th>
                                <th style="min-width: 100px">
                                    <span class="text-dark-75">Actions</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($livres as $livre)
                                <tr>
                                    <td class="pl-0 py-8">
                                        <div class="d-flex align-items-center">
                                            <a class="text-dark-75 font-weight d-block font-size-lg"
                                                href="/livres/{{ $livre->id }}">{{ $livre->title }}</a>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">
                                            {{ $livre->description }}</p>
                                    </td>
                                    <td>
                                        <p class="text-dark-75 font-weight d-block font-size-lg">{{ $livre->nb_page }}
                                        </p>
                                    </td>
                                    <td>
                                        <button wire:click='delete({{$livre->id}})' class="btn btn-sm btn-clean btn-icon" title="Supprimer">
                                            <i class="flaticon-delete text-danger"></i>
                                        </button>
                                        <button wire:click='callModal({{$livre->id}})' class="btn btn-sm btn-clean btn-icon" title="Editer">
                                            <i class="flaticon2-edit text-primary"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

