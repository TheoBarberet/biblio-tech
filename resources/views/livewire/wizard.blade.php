
<div>
    @if(!empty($successMsg))
    <div class="alert alert-success">
        {{ $successMsg }}
    </div>
    @endif
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="multi-wizard-step">
                <a href="#step-1" type="button"
                    class="btn {{ $currentStep != 1 ? 'btn-success' : 'btn-primary' }}">1</a>
                <font size="3"><p style="color:{{ $currentStep != 1 ? 'green' : 'blue' }};"><strong>Votre organisation</strong></p></font>
                <p>Créer un compte personnalisé</p>
            </div>
            <div class="multi-wizard-step">
                    @if ($currentStep >2)
                    <a href="#step-2" type="button"
                    class="btn btn-success">2</a>
                    <font size="3"><p style="color:green;"><strong>Votre école</strong></p></font>
                    <p>Décrivez votre école</p>
                    @else
                    <a href="#step-2" type="button"
                    class="btn {{ $currentStep != 2 ? 'btn-default' : 'btn-primary' }}">2</a>
                    <font size="3"><p style="color:{{ $currentStep != 2 ? 'black' : 'blue' }};"><strong>Votre école</strong></p></font>
                    <p>Décrivez votre école</p>
                    @endif
            </div>
            <div class="multi-wizard-step">
                    @if ($currentStep <3)
                    <a href="#step-3" type="button"
                    class="btn btn-default">3</a>
                    <font size="3"><p style="color:black};"><strong>Votre campus</strong></p></font>
                    <p>Décrivez votre campus</p>
                    @else
                    <a href="#step-3" type="button"
                    class="btn {{ $currentStep != 3 ? 'btn-success' : 'btn-primary' }}">3</a>
                    <font size="3"><p style="color:{{ $currentStep != 3 ? 'green' : 'blue' }};"><strong>Votre campus</strong></p></font>
                    <p>Décrivez votre campus</p>
                    @endif
            </div>
            <div class="multi-wizard-step">
                <a href="#step-4" type="button"
                    class="btn {{ $currentStep != 4 ? 'btn-default' : 'btn-primary' }}"
                    disabled="disabled">4</a>
                    <font size="3"><p style="color:{{ $currentStep != 4 ? 'black' : 'blue' }};"><strong>Résumé</strong></p></font>
                    <p>Examiner et soumettre</p>
            </div>
        </div>
    </div>
    <br>
    <br>
    @if ($currentStep == 1)
    <div class="row setup-content {{ $currentStep != 1 ? 'display-none' : '' }}" id="step-1">
        <div class="col-md-12">
            <h3> Entrez les détails de votre compte</h3>
            <div class="form-group">
                <label for="title">Prénom:</label>
                <input type="text" wire:model="firstname" class="form-control" id="firstname">
                @error('firstname') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="title">Nom de famille:</label>
                <input type="text" wire:model="lastname" class="form-control" id="lastname">
                @error('lastname') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="description">Téléphone:</label>
                <input type="tel" wire:model="phone" class="form-control" id="phone" />
                @error('phone') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" wire:model="email" class="form-control" id="email">
                @error('email') <span class="error">{{ $message }}</span> @enderror
                @error('test') <span class="error">{{ $message }}</span> @enderror
            </div>
            <button class="btn btn-primary nextBtn btn-lg pull-right" wire:click="firstStepSubmit"
                type="button">Next</button>
        </div>
    </div>
    @elseif ($currentStep == 2)
    <div class="row setup-content {{ $currentStep != 2 ? 'display-none' : '' }}" id="step-2">
        <div class="col-md-12">
            <h3> Renseignez votre école</h3>
            <div class="form-group">
                <label for="title">Nom de l'école:</label>
                <input type="text" wire:model="schoolname" class="form-control" id="schoolname">
                @error('schoolname') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="detail">Description de l'école:</label>
                <textarea type="text" wire:model="detail" class="form-control"
                    id="taskDetail"></textarea>
                @error('detail') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="title">Site web de l'école:</label>
                <input type="url" wire:model="website" class="form-control" id="website">
                @error('website') <span class="error">{{ $message }}</span> @enderror
            </div>
            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button"
                wire:click="secondStepSubmit">Next</button>
            <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(1)">Back</button>
        </div>
    </div>
    @elseif ($currentStep == 3)
    <div class="row setup-content {{ $currentStep != 3 ? 'display-none' : '' }}" id="step-3">
        <div class="col-md-12">
            <h3> Renseignez votre campus</h3>
            <div class="form-group">
                <label for="title">Nom du campus:</label>
                <input type="text" wire:model="campusname" class="form-control" id="campusname">
                @error('campusname') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="title">Adresse du campus:</label>
                <input type="text" wire:model="campusadress" class="form-control" id="campusaddress">
                @error('campusadress') <span class="error">{{ $message }}</span> @enderror
            </div>
            <div class="form-group">
                <label for="description">Code postal du campus:</label>
                <input type="text" wire:model="codePostal" class="form-control" id="codePostal" />
                @error('codePostal') <span class="error">{{ $message }}</span> @enderror
            </div>
            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button"
                wire:click="thirdStepSubmit">Next</button>
            <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(1)">Back</button>
        </div>
    </div>
    @else
    <div class="row setup-content {{ $currentStep != 4 ? 'display-none' : '' }}" id="step-4">
        <div class="col-md-12">
            <h3> Récapitulatif</h3>
            <table class="table">
                <tr>
                    <td>Prénom:</td>
                    <td><input type="text" wire:model="firstname" class="form-control" id="firstname" value={{$firstname}}>
                        @error('firstname') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Nom de famille:</td>
                    <td><input type="text" wire:model="lastname" class="form-control" id="lastname" value={{$lastname}}>
                        @error('lastname') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Téléphone:</td>
                    <td><input type="tel" wire:model="phone" class="form-control" id="phone" value={{$phone}}>
                        @error('phone') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input type="email" wire:model="email" class="form-control" id="email" value={{$email}}>
                        @error('email') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Ecole:</td>
                    <td><input type="text" wire:model="schoolname" class="form-control" id="schoolname" value={{$schoolname}}>
                        @error('schoolname') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td><input type="detail" wire:model="detail" class="form-control" id="detail" value={{$detail}}>
                        @error('detail') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Site web:</td>
                    <td><input type="url" wire:model="website" class="form-control" id="website" value={{$website}}>
                        @error('website') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Campus:</td>
                    <td><input type="text" wire:model="campusname" class="form-control" id="campusname" value={{$campusname}}>
                        @error('campusname') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Adresse:</td>
                    <td><input type="text" wire:model="campusadress" class="form-control" id="campusadress" value={{$campusadress}}>
                        @error('campusadress') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
                <tr>
                    <td>Code postal:</td>
                    <td><input type="text" wire:model="codePostal" class="form-control" id="codePostal" value={{$codePostal}}>
                        @error('codePostal') <span class="error">{{ $message }}</span> @enderror</td>
                </tr>
            </table>
            <button class="btn btn-success btn-lg pull-right" wire:click="submitForm" type="button">Finish!</button>
            <button class="btn btn-danger nextBtn btn-lg pull-right" type="button" wire:click="back(2)">Back</button>
        </div>
    </div>
    @endif
</div>
<script src="intlTelInput.js"></script>
<script>
  var input = document.querySelector("#phone");
  window.intlTelInput(input, {
    // whether or not to allow the dropdown
allowDropdown: true,

// if there is just a dial code in the input: remove it on blur, and re-add it on focus
autoHideDialCode: true,

// add a placeholder in the input with an example number for the selected country
autoPlaceholder: "polite",

// modify the auto placeholder
customPlaceholder: null,

// append menu to specified element
dropdownContainer: null,

// don't display these countries
excludeCountries: [],

// format the input value during initialisation and on setNumber
formatOnDisplay: true,

// geoIp lookup function
geoIpLookup: null,

// inject a hidden input with this name, and on submit, populate it with the result of getNumber
hiddenInput: "",

// initial country
initialCountry: "",

// localized country names e.g. { 'de': 'Deutschland' }
localizedCountries: null,

// don't insert international dial codes
nationalMode: true,

// display only these countries
onlyCountries: [],

// number type to use for placeholders
placeholderNumberType: "MOBILE",

// the countries at the top of the list. defaults to united states and united kingdom
preferredCountries: [ "fr","be","ch", "lu" ],

// display the country dial code next to the selected flag so it's not part of the typed number
separateDialCode:true,

// specify the path to the libphonenumber script to enable validation/formatting
utilsScript: ""
  });
</script>


