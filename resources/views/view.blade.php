@extends('layout.default')
@section('content')
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">
                Informations sur le livre
            </h3>
        </div>
    </div>
    <div class="card-body">
            <p><b>Titre :</b> {{$livre->title}}</p>
            <p><b>Description :</b> {{$livre->description}}</p>
            <p><b>Nombre de pages :</b> {{$livre->nb_page}}</p>
            <div class="d-flex justify-content border-top mt-5 pt-10">
                <div class="mr-2">
                    <form method="POST" action="/buy/{{$livre->id}}">
                        @csrf
                    <button class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">💳  Acheter</button>
                </form>
                </div>
        <div>
            <button class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" onclick="window.location='{{ route('livres.edit', ['livre' => $livre->id]) }}'">📝 Edit</button>
        </div>
            </div>
</div>
</div>
@endsection
