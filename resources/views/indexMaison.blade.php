@extends('layout.default')
@section('styles')
@livewireStyles
<style>
    table
{
    border-collapse: collapse;
}
td, th /* Mettre une bordure sur les td ET les th */
{
    border: 1px solid black;
}
</style>
@endsection
@section('content')
@livewire('maisons-table')



@endsection
@section('scripts')
@livewireScripts
@endsection


