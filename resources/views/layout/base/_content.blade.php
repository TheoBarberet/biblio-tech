{{-- Content --}}
@if (config('layout.content.extended'))
    @yield('content')
@else
    <div class="d-flex flex-column-fluid">
        <div class="{{ Metronic::printClasses('content-container', false) }}">
            @yield('content')
            <a href="{{ url()->previous() }}" class="btn btn-danger font-weight-bolder">Retour</a>
        </div>
    </div>
@endif
