@extends('layout.default')
@section('content')
@if(isset($livre))
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">
                Modifier le livre
            </h3>
        </div>
    </div>
    <div class="card-body">
    <form method="POST" action="/livres/{{$livre->id}}">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Titre du livre :</label>
            <div class="col-lg-9 col-xl-6">
                <input class="form-control form-control-lg form-control-solid is-valid" id="title" name="title" type="text" placeholder="Titre" value="{{$livre->title}}">
                @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Description du livre :</label>
            <div class="col-lg-9 col-xl-6">
                <input class="form-control form-control-lg form-control-solid" name="description" type="text" placeholder="Description" value="{{$livre->description}}">
                @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Nombre de pages du livre :</label>
            <div class="col-lg-9 col-xl-6">
                <input class="form-control form-control-lg form-control-solid" name="nb_page" type="text" placeholder="Nombre de pages" value="{{$livre->nb_page}}">
                @error('nb_page')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        @if (Auth::user()->id == $livre->user_id)
        <div class="d-flex justify-content border-top mt-5 pt-10">
            <div class="mr-2">
        <button class="btn btn-warning font-weight-bolder text-uppercase px-9 py-4">↻ Mettre à jour</button>
            </form>
        </div>
        <div>
            <form method="POST" action="/livres/{{$livre->id}}">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger font-weight-bolder text-uppercase px-9 py-4">✖ Supprimer</button>
            </form>
        </div>
        </div>
        @endif
    </div>
</div>
@else
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">
                Créer un livre
            </h3>
        </div>
    </div>
    <div class="card-body">
    <form method="POST" action={{ '/livres' }}>
        @csrf
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Titre du livre :</label>
            <div class="col-lg-9 col-xl-6">
                <input class="form-control form-control-lg form-control-solid" id="titre_livre" name="title" type="text" placeholder="Titre" onkeydown="checkTitle()">
                @error('title')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Description du livre :</label>
            <div class="col-lg-9 col-xl-6">
                <input class="form-control form-control-lg form-control-solid" id="description_livre" name="description" type="text" placeholder="Description" onkeydown="checkDescription()">
                @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">Nombre de pages du livre :</label>
            <div class="col-lg-9 col-xl-6">
                <input class="form-control form-control-lg form-control-solid" id="nb_pages_livre" name="nb_page" type="text" placeholder="Nombre de pages" onkeydown="checkPages()">
                @error('nb_page')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
    <button class="btn btn-dark font-weight-bolder text-uppercase px-9 py-4" type="submit">Créer ce livre</button>
    </form>
    </div>
</div>
@endif
@endsection
@section('scripts')
<script>
    function checkTitle()
{
    setTimeout(function(){
    const length = $("#titre_livre").val().length;
    if(length>=6){
        $("#titre_livre").addClass("is-valid");
    }
    else{
        $("#titre_livre").removeClass("is-valid");
    }
    }, 20);
}

    function checkDescription()
{
    setTimeout(function(){
    const length = $("#description_livre").val().length;
    if(length>=10){
        $("#description_livre").addClass("is-valid");
    }
    else{
        $("#description_livre").removeClass("is-valid");
    }
    }, 20);
}

    function checkPages()
{
    setTimeout(function(){
    const length = $("#nb_pages_livre").val().length;
    let val = document.getElementById("nb_pages_livre").value
    let isnum = /^\d+$/.test(val);
    if(length==3){
        if(isnum){
        $("#nb_pages_livre").addClass("is-valid");
        }
        else{
        $("#nb_pages_livre").removeClass("is-valid");
        }
    }
    else{
        $("#nb_pages_livre").removeClass("is-valid");
    }
    }, 20);
}

</script>
@endsection
