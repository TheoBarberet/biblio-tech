<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @livewireStyles
    <link rel="stylesheet" href="intlTelInput.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap">
    <link href="{{ asset('multiform.css') }}" rel="stylesheet" id="bootstrap">
    @extends('layout.default')
@section('content')
<body class="mt-5">
    <div class="container">
        <div class="text-center">
            Création école Edukare
        </div>
        <br>
        <livewire:wizard />
    </div>
</body>
@endsection
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
@livewireScripts
</html>
