@extends('layout.default')
@section('styles')
@livewireStyles
<style>
    table
{
    border-collapse: collapse;
}
td, th /* Mettre une bordure sur les td ET les th */
{
    border: 1px solid black;
}
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card card-custom gutter-b">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">
                        Informations sur la maison
                    </h3>
                </div>
            </div>
            <div class="card-body">
                @livewire('edit-maison', ['maison' => $maison])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        @livewire('livres-maison', ['maison' => $maison])
    </div>
</div>
@endsection
@section('scripts')
@livewireScripts
@endsection



