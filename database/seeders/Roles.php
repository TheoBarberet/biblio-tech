<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [
                'name'       => 'superuser',
                'guard_name' => 'web'
            ],
            [
                'name'       => 'user',
                'guard_name' => 'web'
            ],
        ]);
    }
}
