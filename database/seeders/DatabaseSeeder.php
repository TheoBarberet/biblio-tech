<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TypeSeeder::class);
        $this->call(Roles::class);
        $this->call(UserSeeder::class);
        $this->call(LivreSeeder::class);
        $this->call(MaisonSeeder::class);
        $this->call(LinkSeeder::class);
    }
}
