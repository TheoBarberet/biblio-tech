<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MaisonEdition;

class MaisonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MaisonEdition::create([
            'name' => 'Maison du livre',
            'slogan' => 'slogan test',
            'creator_id' => '1',
            ]);
    }
}
