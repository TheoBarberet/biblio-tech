<?php

namespace Database\Seeders;

use App\Models\LinkMaisonUser;
use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LinkMaisonUser::create([
            'user_id' => '1',
            'maison_id' => '1',
            ]);
    }
}
