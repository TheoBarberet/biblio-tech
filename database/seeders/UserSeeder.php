<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Théo',
            'email' => 'theo.barberet@gmail.com',
            'password' => Hash::make('password'),
            ])->assignRole('superuser');

            User::create([
                'name' => 'Fabien',
            'email' => 'fabien@gmail.com',
            'password' => Hash::make('password'),
                ])->assignRole('user');

                User::create([
                    'name' => 'Quentin',
                    'email' => 'quentin@gmail.com',
                    'password' => Hash::make('password'),
                    ])->assignRole('user');

                    User::create([
                        'name' => Str::random(10),
                        'email' => Str::random(10).'@gmail.com',
                        'password' => Hash::make('password'),
                        ])->assignRole('user');
                        User::factory()->count(20)->create();
    }
}
