<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Type;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::create([
            'name' => 'Roman',
        ]);
        Type::create([
            'name' => 'SF',
        ]);
        Type::create([
            'name' => 'BD',
        ]);
        Type::create([
            'name' => 'Jeunesse',
        ]);
        Type::create([
            'name' => 'Educatif',
        ]);
    }
}
