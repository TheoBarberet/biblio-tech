<?php

namespace Database\Factories;

use App\Models\Livre;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Type;
use Illuminate\Support\Arr;

class LivreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Livre::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $alltype = Type::all();
        $listType = [];
        foreach($alltype as $type){
            array_push($listType,$type->name);
        }
        return [
            'title' => $this->faker->sentence(1),
            'description' => $this->faker->sentence,
            'user_id' => '1',
            'nb_page' => $this->faker->numberBetween(100,999),
            'maison_id' => '1',
            'price' => $this->faker->numberBetween(5,50),
            'type' => Arr::random($listType),
        ];
    }
}
