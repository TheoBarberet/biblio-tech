<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->integer('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('schoolname')->nullable();
            $table->longText('detail')->nullable();
            $table->string('website')->nullable();
            $table->string('campusname')->nullable();
            $table->string('campusadress')->nullable();
            $table->integer('codePostal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
